// GlobalParameters.qml代码
pragma Singleton

import QtQuick 2.12

Item {

    enum Status {
        Idle,
        Off,
        On
    }

    readonly property color btnBkColor: "lightblue"
    readonly property int txtPointSize: 12
}
