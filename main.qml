// main.qml代码
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQml 2.12
// 使用相对路径方式调用组件
import "./CustomComponents"
import "./CustomComponents/SubDir"

// 使用模块注册方式步骤1：导入模块
import Parameters 1.0

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    // 使用相对路径方式调用组件DeTextField
    DeTextField {
        id: text
        anchors.centerIn: parent
        width: 200
        height: 50
        font.pointSize: GlobalParameters.txtPointSize           // 使用已注册的模组内单例组件属性
    }

    // 使用相对路径方式调用组件DeButton
    DeButton {
        anchors.top: text.bottom
        anchors.topMargin: 25
        anchors.horizontalCenter: parent.horizontalCenter
        width: 150
        height: 50
        btnBkColor: GlobalParameters.btnBkColor                 // 使用已注册的模组内单例组件属性
        text: qsTr("ok")
    }

    Component.onCompleted: {
        // 使用已注册的模组内单例组件的枚举类型
        console.log(GlobalParameters.Status.On)
    }
}
