import QtQuick 2.12
import QtQuick.Controls 2.12

Button {
    id: _button
    implicitHeight: 80
    implicitWidth: 130
    font.pixelSize: _button.pressed ? 22 : 24

    property color btnBkColor: "lightblue"
    property color btnTxtColor: "#ffffff"

    background: Rectangle {
        color: _button.enabled ? (_button.checkable ? (_button.checked ?  Qt.darker(btnBkColor) : btnBkColor) : _button.pressed ? Qt.darker(btnBkColor) : btnBkColor) : "gray"
        radius: 8
    }

    contentItem: Text {
        text: parent.text
        font: parent.font
        color: btnTxtColor
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }
}
