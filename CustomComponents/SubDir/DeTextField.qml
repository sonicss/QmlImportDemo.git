import QtQuick 2.12
import QtQuick.Controls 2.12

TextField {
    implicitHeight: 40
    implicitWidth: 178
    horizontalAlignment: Text.AlignHCenter
    font.pointSize: 13

    color: acceptableInput ? enabled ? "black" : "gray" : "red"


    background: Rectangle {
        anchors.fill: parent
        color: enabled ? "white" : "lightgray"
        border.color: readOnly ? "lightgray" : parent.activeFocus ? "blue" : "lightgray"
        border.width: readOnly ? 1 : parent.activeFocus ? 2 : 1
        radius: 5
    }
}
